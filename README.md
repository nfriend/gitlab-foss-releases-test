# GitLab FOSS Releases Test

A project to test the automatic release post > Releases page pipeline step in [www-gitlab-com](https://gitlab.com/gitlab-com/www-gitlab-com/).

See the result here: https://gitlab.com/nfriend/gitlab-foss-releases-test/-/releases
